<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Pengguna: role Admin
        DB::table('pengguna')->insert([
            'username' => 'admin',
            'password' => \Hash::make('sandiaman'),
            'role' => 'admin',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);

        // Pengguna: role Pegawai
        $faker = Faker::create();
        foreach (range(1, 5) as $index) {
            DB::table('pengguna')->insert([
                'username' => $faker->username,
                'password' => \Hash::make('sandiaman'),
                'role' => 'pegawai',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]);
        }

        // Menu
        $menus = array(
            array(
                'code' => 'view-master-pelanggan',
                'name' => 'Lihat Master Pelanggan',
                'group' => 'master-pelanggan',
            ),
            array(
                'code' => 'create-master-pelanggan',
                'name' => 'Tambah Master Pelanggan',
                'group' => 'master-pelanggan',
            ),
            array(
                'code' => 'edit-master-pelanggan',
                'name' => 'Ubah Master Pelanggan',
                'group' => 'master-pelanggan',
            ),
            array(
                'code' => 'delete-master-pelanggan',
                'name' => 'Hapus Master Pelanggan',
                'group' => 'master-pelanggan',
            ),

            array(
                'code' => 'view-master-supplier',
                'name' => 'Lihat Master Supplier',
                'group' => 'master-supplier',
            ),
            array(
                'code' => 'create-master-supplier',
                'name' => 'Tambah Master Supplier',
                'group' => 'master-supplier',
            ),
            array(
                'code' => 'edit-master-supplier',
                'name' => 'Ubah Master Supplier',
                'group' => 'master-supplier',
            ),
            array(
                'code' => 'delete-master-supplier',
                'name' => 'Hapus Master Supplier',
                'group' => 'master-supplier',
            ),

            array(
                'code' => 'view-master-pengguna',
                'name' => 'Lihat Master Pengguna',
                'group' => 'master-pengguna',
            ),
            array(
                'code' => 'create-master-pengguna',
                'name' => 'Tambah Master Pengguna',
                'group' => 'master-pengguna',
            ),
            array(
                'code' => 'edit-master-pengguna',
                'name' => 'Ubah Master Pengguna',
                'group' => 'master-pengguna',
            ),
            array(
                'code' => 'delete-master-pengguna',
                'name' => 'Hapus Master Pengguna',
                'group' => 'master-pengguna',
            ),

            array(
                'code' => 'view-master-kategori',
                'name' => 'Lihat Master Kategori',
                'group' => 'master-kategori',
            ),
            array(
                'code' => 'create-master-kategori',
                'name' => 'Tambah Master Kategori',
                'group' => 'master-kategori',
            ),
            array(
                'code' => 'edit-master-kategori',
                'name' => 'Ubah Master Kategori',
                'group' => 'master-kategori',
            ),
            array(
                'code' => 'delete-master-kategori',
                'name' => 'Hapus Master Kategori',
                'group' => 'master-kategori',
            ),

            array(
                'code' => 'view-master-satuan',
                'name' => 'Lihat Master Satuan',
                'group' => 'master-satuan',
            ),
            array(
                'code' => 'create-master-satuan',
                'name' => 'Tambah Master Satuan',
                'group' => 'master-satuan',
            ),
            array(
                'code' => 'edit-master-satuan',
                'name' => 'Ubah Master Satuan',
                'group' => 'master-satuan',
            ),
            array(
                'code' => 'delete-master-satuan',
                'name' => 'Hapus Master Satuan',
                'group' => 'master-satuan',
            ),

            array(
                'code' => 'view-master-produk',
                'name' => 'Lihat Master Produk',
                'group' => 'master-produk',
            ),
            array(
                'code' => 'create-master-produk',
                'name' => 'Tambah Master Produk',
                'group' => 'master-produk',
            ),
            array(
                'code' => 'edit-master-produk',
                'name' => 'Ubah Master Produk',
                'group' => 'master-produk',
            ),
            array(
                'code' => 'delete-master-produk',
                'name' => 'Hapus Master Produk',
                'group' => 'master-produk',
            ),

            array(
                'code' => 'view-transaksi-pembelian',
                'name' => 'Lihat Transaksi Pembelian',
                'group' => 'transaksi-pembelian',
            ),
            array(
                'code' => 'create-transaksi-pembelian',
                'name' => 'Tambah Transaksi Pembelian',
                'group' => 'transaksi-pembelian',
            ),
            array(
                'code' => 'edit-transaksi-pembelian',
                'name' => 'Ubah Transaksi Pembelian',
                'group' => 'transaksi-pembelian',
            ),
            array(
                'code' => 'delete-transaksi-pembelian',
                'name' => 'Hapus Transaksi Pembelian',
                'group' => 'transaksi-pembelian',
            ),

            array(
                'code' => 'view-transaksi-penjualan',
                'name' => 'Lihat Transaksi Penjualan',
                'group' => 'transaksi-penjualan',
            ),
            array(
                'code' => 'create-transaksi-penjualan',
                'name' => 'Tambah Transaksi Penjualan',
                'group' => 'transaksi-penjualan',
            ),
            array(
                'code' => 'edit-transaksi-penjualan',
                'name' => 'Ubah Transaksi Penjualan',
                'group' => 'transaksi-penjualan',
            ),
            array(
                'code' => 'delete-transaksi-penjualan',
                'name' => 'Hapus Transaksi Penjualan',
                'group' => 'transaksi-penjualan',
            ),

            array(
                'code' => 'view-transaksi-returjual',
                'name' => 'Lihat Transaksi Returjual',
                'group' => 'transaksi-returjual',
            ),
            array(
                'code' => 'create-transaksi-returjual',
                'name' => 'Tambah Transaksi Returjual',
                'group' => 'transaksi-returjual',
            ),
            array(
                'code' => 'edit-transaksi-returjual',
                'name' => 'Ubah Transaksi Returjual',
                'group' => 'transaksi-returjual',
            ),
            array(
                'code' => 'delete-transaksi-returjual',
                'name' => 'Hapus Transaksi Returjual',
                'group' => 'transaksi-returjual',
            ),

            array(
                'code' => 'view-transaksi-pengiriman',
                'name' => 'Lihat Transaksi Pengiriman',
                'group' => 'transaksi-pengiriman',
            ),
            array(
                'code' => 'create-transaksi-pengiriman',
                'name' => 'Tambah Transaksi Pengiriman',
                'group' => 'transaksi-pengiriman',
            ),
            array(
                'code' => 'edit-transaksi-pengiriman',
                'name' => 'Ubah Transaksi Pengiriman',
                'group' => 'transaksi-pengiriman',
            ),
            array(
                'code' => 'delete-transaksi-pengiriman',
                'name' => 'Hapus Transaksi Pengiriman',
                'group' => 'transaksi-pengiriman',
            ),

            array(
                'code' => 'view-transaksi-pembayaran',
                'name' => 'Lihat Transaksi Pembayaran',
                'group' => 'transaksi-pembayaran',
            ),
            array(
                'code' => 'create-transaksi-pembayaran',
                'name' => 'Tambah Transaksi Pembayaran',
                'group' => 'transaksi-pembayaran',
            ),
            array(
                'code' => 'edit-transaksi-pembayaran',
                'name' => 'Ubah Transaksi Pembayaran',
                'group' => 'transaksi-pembayaran',
            ),
            array(
                'code' => 'delete-transaksi-pembayaran',
                'name' => 'Hapus Transaksi Pembayaran',
                'group' => 'transaksi-pembayaran',
            ),

            array(
                'code' => 'view-laporan-pembelian',
                'name' => 'Lihat Laporan Pembelian',
                'group' => 'laporan',
            ),
            array(
                'code' => 'view-laporan-penjualan',
                'name' => 'Lihat Laporan Penjualan',
                'group' => 'laporan',
            ),
            array(
                'code' => 'view-laporan-returjual',
                'name' => 'Lihat Laporan Returjual',
                'group' => 'laporan',
            ),
            array(
                'code' => 'view-laporan-pengiriman',
                'name' => 'Lihat Laporan Pengiriman',
                'group' => 'laporan',
            ),
            array(
                'code' => 'view-laporan-pembayaran',
                'name' => 'Lihat Laporan Pembayaran',
                'group' => 'laporan',
            ),
        );
        foreach ($menus as $item) {
            DB::table('menu')->insert([
                'code' => $item['code'],
                'name' => $item['name'],
                'group' => $item['group'],
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]);
        }

        // Satuan
        $satuan = array(
            'Biji', //ID: 1
            'Kotak', //ID: 2
            'Buah', //ID: 3
            'Cm', //ID: 4
        );
        foreach ($satuan as $name) {
            DB::table('satuan')->insert([
                'name' => $name,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]);
        }

        // Kategori Produk
        $kategoriProduk = array(
            'Lensa', //ID: 1
            'Frame', //ID: 2
            'Aksesoris', //ID: 3
        );
        foreach ($kategoriProduk as $name) {
            DB::table('kategori')->insert([
                'name' => $name,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]);
        }

        // Hak Akses Pengguna
        foreach (range(2, 6) as $penggunaID) {
            foreach ($menus as $menuID => $menu) {
                DB::table('hakakses')->insert([
                    'pengguna_id' => $penggunaID,
                    'menu_id' => ($menuID + 1),
                    'is_allowed' => FALSE,
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                ]);
            }
        }

        // Supplier
        $faker = Faker::create();
        foreach (range(1, 5) as $index) {
            DB::table('suplier')->insert([
                'code' => 'SUP'.$index,
                'name' => $faker->name,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]);
        }

        // Pelanggan
        $faker = Faker::create();
        foreach (range(1, 5) as $index) {
            DB::table('pelanggan')->insert([
                'code' => 'PEL'.$index,
                'name' => $faker->name,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]);
        }

        // Produk
        $faker = Faker::create();
        foreach (range(1, 5) as $index) {
            DB::table('produk')->insert([
                'kategori_id' => $faker->numberBetween(1, 3),
                'satuan_id' => $faker->numberBetween(1, 4),
                'code' => 'PRO'.$index,
                'name' => $faker->name,
                'price_buy' => $faker->numberBetween(100000, 300000),
                'price_sell' => $faker->numberBetween(500000, 500000),
                'stock' => $faker->randomDigitNotNull,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]);
        }
    }
}
