<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturjualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('returjual', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 20);
            $table->integer('penjualan_id')->unsigned();
            $table->integer('pengguna_id')->unsigned();
            $table->datetime('date');
            $table->integer('price_total');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('returjual');
    }
}
