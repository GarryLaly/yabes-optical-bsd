<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturjualDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('returjual_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('returjual_id')->unsigned();
            $table->integer('produk_id')->unsigned();
            $table->string('produk_name', 30);
            $table->integer('produk_price');
            $table->integer('qty');
            $table->integer('price_subtotal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('returjual_detail');
    }
}
