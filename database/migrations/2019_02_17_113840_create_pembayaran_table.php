<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penjualan_id')->unsigned()->nullable();
            $table->integer('pembelian_id')->unsigned()->nullable();
            $table->string('code', 20);
            $table->datetime('date');
            $table->integer('amount_pay');
            $table->integer('amount_left');
            $table->integer('amount_total');
            $table->enum('type', array('penjualan', 'pembelian'));
            $table->boolean('is_paidoff')->default(FALSE);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran');
    }
}
