<html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Laporan Laba Rugi</title>
  <style>
  @page { margin: 20px; }
  .page-break {
    page-break-after: always;
  }
  </style>
</head><body style="padding: 0 50px;">
<br>
<br>
<br>
<br>
<br>
<div style="text-align: center; margin-bottom: 25px;">
<strong style="font-size: 24px;">LAPORAN LABA RUGI</strong>
</div>
<div style="margin-bottom: 5px;">
Periode Laporan pada <strong>{{ date('d F Y', strtotime($date_start)) }} sampai {{ date('d F Y', strtotime($date_end)) }}</strong>
</div>
<table border="1" cellspacing="0" cellpadding="10" width="100%">
    <?php
      $sell_total = 0;
      $buy_total = 0;
      $piutangAmountPay = 0;
      $piutangSisaPay = 0;
      $hutangAmountPay = 0;
      $hutangSisaPay = 0;
      $pembelianGrandTotal = 0;


      foreach ($pembelian as $item) {
        foreach ($item->pembayaran as $itemPembayaran) {
          $hutangAmountPay += $itemPembayaran->amount_pay;
        }

        $pembelianGrandTotal += $item->price_total;
      }

      $hutangSisaPay = $pembelianGrandTotal - $hutangAmountPay;

      foreach ($data as $item) {
        $sellPrice = $item->produk ? $item->produk->price_sell : 0;
        $sellPrice = $sellPrice * $item->qty;
        $sell_total += $sellPrice;

        $buyPrice = $item->produk ? $item->produk->price_buy : 0;
        $buyPrice = $buyPrice * $item->qty;
        $buy_total += $buyPrice;

        foreach ($item->penjualan->pembayaran as $itemPembayaran) {
          $piutangAmountPay += $itemPembayaran->amount_pay;
        }
      }
    ?>
    <?php $piutangSisaPay = $sell_total - $piutangAmountPay; ?>
    <tbody>
      <tr>
        <td>
          <strong>TOTAL PENDAPATAN DARI PENJUALAN</strong>
        </td>
        <td align="right">
          <strong>{{ currencyFormat($sell_total) }}</strong>
        </td>
      </tr>
      <tr>
        <td>
          <strong>TOTAL TAGIHAN PIUTANG DARI PELANGGAN</strong>
        </td>
        <td align="right">
          <strong>{{ currencyFormat($piutangSisaPay) }}</strong>
        </td>
      </tr>
      <tr>
        <td>
          <strong>TOTAL TAGIHAN HUTANG KE SUPPLIER</strong>
        </td>
        <td align="right">
          <strong>{{ currencyFormat($hutangSisaPay) }}</strong>
        </td>
      </tr>
      <tr>
        <td>
          <strong>TOTAL PENGELUARAN DARI PENERIMAAN PEMBELIAN</strong>
        </td>
        <td align="right">
          <strong>{{ currencyFormat($buy_total) }}</strong>
        </td>
      </tr>
      <tr>
        <td>
          <strong>TOTAL LABA BERSIH</strong>
        </td>
        <td align="right">
          <strong>{{ currencyFormat($sell_total - $piutangSisaPay - $hutangSisaPay - $buy_total) }}</strong>
        </td>
      </tr>
    </tbody>
</table>
<div style="text-align: right;margin-top: 20px;">
Salam,
<br>
Admin Yabes Optical
</div>
</body></html>
