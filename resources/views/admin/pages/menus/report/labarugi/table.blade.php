@extends('admin.master')

@section('content')
<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Laporan Laba Rugi</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item active">Laporan Laba Rugi</li>		
    </ol>
  </div>
  <div class="col-lg-6 align-self-center text-right">
    <a href="{{ url('laporan/labarugi/print?date_range='.$date_range) }}" class="btn btn-warning box-shadow btn-icon btn-rounded"><i class="fa fa-print"></i> Cetak Laporan</a>
  </div>
</div>

<section class="main-content">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        Laporan Laba Rugi
        <div class="pull-right">
          <form method="get" action="">
            <label for="date_range" class="pull-right">Filter Tanggal</label>
            <div class="input-group">
              <input type="text" name="date_range" id="date_range" class="form-control pull-right datepicker" style="min-width: 100px;" value="">
              <div class="input-group-append">
                <button type="submit" class="btn btn-primary">Filter</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="card-body">
        <h3>Transaksi Penjualan Produk</h3>
        <table class="table table-striped dt-responsive wrap">
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th>Transaksi Penjualan</th>
                    <th>Nama Produk</th>
                    <th>Kategori</th>
                    <th>Satuan</th>
                    <th>Total Penjualan</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                  $sell_total = 0;
                  $buy_total = 0;
                  $piutangAmountPay = 0;
                  $piutangSisaPay = 0;
                  $hutangAmountPay = 0;
                  $hutangSisaPay = 0;
                  $pembelianGrandTotal = 0;


                  foreach ($pembelian as $item) {
                    foreach ($item->pembayaran as $itemPembayaran) {
                      $hutangAmountPay += $itemPembayaran->amount_pay;
                    }

                    $pembelianGrandTotal += $item->price_total;
                  }

                  $hutangSisaPay = $pembelianGrandTotal - $hutangAmountPay;
                  ?>
                @foreach($data as $item)
                    <?php
                      $sellPrice = $item->produk ? $item->produk->price_sell : 0;
                      $sellPrice = $sellPrice * $item->qty;
                      $sell_total += $sellPrice;

                      $buyPrice = $item->produk ? $item->produk->price_buy : 0;
                      $buyPrice = $buyPrice * $item->qty;
                      $buy_total += $buyPrice;

                      foreach ($item->penjualan->pembayaran as $itemPembayaran) {
                        $piutangAmountPay += $itemPembayaran->amount_pay;
                      }
                    ?>
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>
                          <a href="{{ url('transaksi/pengiriman/'.$item->penjualan->id) }}">{{ $item->penjualan->code }}</a>
                        </td>
                        <td>{{ $item->produk ? $item->produk->name : "-" }}</td>
                        <td>{{ $item->produk->kategori ? $item->produk->kategori->name : "-" }}</td>
                        <td>{{ $item->produk->satuan ? $item->produk->satuan->name : "-" }}</td>
                        <td align="right">{{ currencyFormat($sellPrice) }}</td>
                        <td>
                            <a href="{{ url('transaksi/penjualan/'.$item->penjualan_id) }}" class="btn btn-primary">Detail</a>
                        </td>
                    </tr>
                @endforeach
                <?php $piutangSisaPay = $sell_total - $piutangAmountPay; ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="5">
                  <strong>TOTAL PENDAPATAN DARI PENJUALAN</strong>
                </td>
                <td align="right">
                  <strong>{{ currencyFormat($sell_total) }}</strong>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="5">
                  <strong>TOTAL TAGIHAN PIUTANG DARI PELANGGAN</strong>
                </td>
                <td align="right">
                  <strong>{{ currencyFormat($piutangSisaPay) }}</strong>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="5">
                  <strong>TOTAL TAGIHAN HUTANG KE SUPPLIER</strong>
                </td>
                <td align="right">
                  <strong>{{ currencyFormat($hutangSisaPay) }}</strong>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="5">
                  <strong>TOTAL PENGELUARAN DARI PENERIMAAN PEMBELIAN</strong>
                </td>
                <td align="right">
                  <strong>{{ currencyFormat($buy_total) }}</strong>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="5">
                  <strong>TOTAL LABA BERSIH</strong>
                </td>
                <td align="right">
                  <strong>{{ currencyFormat($sell_total - $piutangSisaPay - $hutangSisaPay - $buy_total) }}</strong>
                </td>
                <td>&nbsp;</td>
              </tr>
            </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('.datepicker').daterangepicker({
      locale: {
        format: 'YYYY/MM/DD'
      }
    });
});

@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection