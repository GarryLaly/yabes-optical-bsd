<html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Laporan Pembayaran</title>
  <style>
  @page { margin: 20px; }
  .page-break {
    page-break-after: always;
  }
  </style>
</head><body style="padding: 0 50px;">
<br>
<br>
<br>
<br>
<br>
<div style="text-align: center; margin-bottom: 25px;">
<strong style="font-size: 24px;">LAPORAN PEMBAYARAN</strong>
</div>
<div style="margin-bottom: 5px;">
Tanggal Transaksi pada <strong>{{ date('d F Y', strtotime($date_start)) }} sampai {{ date('d F Y', strtotime($date_end)) }}</strong>
</div>
<table border="1" cellspacing="0" cellpadding="10" width="100%">
    <thead>
    <tr>
        <td width="10" align="center">
        <strong>Code</strong>
        </td>
        <td width="10" align="center">
        <strong>Transaksi</strong>
        </td>
        <td width="80" align="center">
        <strong>Tanggal Bayar</strong>
        </td>
        <td align="center">
        <strong>Bayar</strong>
        </td>
        <td align="center">
        <strong>Sisa</strong>
        </td>
        <td align="center">
        <strong>Total Tagihan</strong>
        </td>
    </tr>
    </thead>
    <tbody>
    <?php
    $grand_amount_pay = 0;
    $grand_amount_left = 0;
    $grand_amount_total = 0;
    ?>
    @foreach($pembayaran as $item)
    <tr>
        <td>{{ $item->code }}</td>
        <td align="center">
          <a href="{{ $item->penjualan_id ? url('transaksi/penjualan/'.$item->penjualan_id) : url('transaksi/pembelian/'.$item->pembelian_id) }}">{{ $item->penjualan_id ? $item->penjualan->code : $item->pembelian->code }}</a>
        </td>
        <td align="center">{{ date('Y-m-d', strtotime($item->date)) }}</td>
        <td align="right">{{ currencyFormat($item->amount_pay) }}</td>
        <td align="right">{{ currencyFormat($item->amount_left) }}</td>
        <td align="right">{{ currencyFormat($item->amount_total) }}</td>
    </tr>
    <?php
    $grand_amount_pay += $item->amount_pay;
    $grand_amount_left += $item->amount_left;
    $grand_amount_total += $item->amount_total;
    ?>
    @endforeach
    <tr>
        <td colspan="3" align="center">
        <strong>TOTAL</strong>
        </td>
        <td align="right">{{ currencyFormat($grand_amount_pay) }}</td>
        <td align="right">{{ currencyFormat($grand_amount_left) }}</td>
        <td align="right">{{ currencyFormat($grand_amount_total) }}</td>
    </tr>
    </tbody>
</table>
<div style="text-align: right;margin-top: 20px;">
Salam,
<br>
Admin Yabes Optical
</div>
</body></html>
