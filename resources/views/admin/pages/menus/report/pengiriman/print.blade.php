<html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Laporan Pengiriman</title>
  <style>
  @page { margin: 20px; }
  .page-break {
    page-break-after: always;
  }
  </style>
</head><body style="padding: 0 50px;">
<br>
<br>
<br>
<br>
<br>
<div style="text-align: center; margin-bottom: 25px;">
<strong style="font-size: 24px;">LAPORAN PENGIRIMAN</strong>
</div>
<div style="margin-bottom: 5px;">
Tanggal Transaksi pada <strong>{{ date('d F Y', strtotime($date_start)) }} sampai {{ date('d F Y', strtotime($date_end)) }}</strong>
</div>
<table border="1" cellspacing="0" cellpadding="10" width="100%">
    <thead>
    <tr>
        <td align="center">
        <strong>Code</strong>
        </td>
        <td align="center">
        <strong>Code Penjualan</strong>
        </td>
        <td align="center">
        <strong>Pelanggan</strong>
        </td>
        <td align="center">
        <strong>No. HP</strong>
        </td>
        <td align="center">
        <strong>Alamat Tujuan</strong>
        </td>
        <td align="center">
        <strong>Tanggal Pengiriman</strong>
        </td>
        <td align="center">
        <strong>Status Pengiriman</strong>
        </td>
    </tr>
    </thead>
    <tbody>
    @foreach($pengiriman as $item)
    <tr>
        <td align="center">{{ $item->pengiriman ? $item->pengiriman->code : '-' }}</td>
        <td align="center">{{ $item->penjualan ? $item->penjualan->code : '-' }}</td>
        <td align="center">{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->name : '-' }}</td>
        <td align="center">{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->phone : '-' }}</td>
        <td align="center">{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->address : '-' }}</td>
        <td align="center">{{ date('Y-m-d', strtotime($item->pengiriman->date)) }}</td>
        <td align="center">{{ $item->is_delivered == "1" ? "Terkirim" : "Proses" }}</td>
    </tr>
    @endforeach
    </tbody>
</table>
<div style="text-align: right;margin-top: 20px;">
Salam,
<br>
Admin Yabes Optical
</div>
</body></html>
