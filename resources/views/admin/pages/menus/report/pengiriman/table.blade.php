@extends('admin.master')

@section('content')
<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Laporan Pengiriman</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item active">Laporan Pengiriman</li>		
    </ol>
  </div>
  <div class="col-lg-6 align-self-center text-right">
    <a href="{{ url('laporan/pengiriman/print?date_range='.$date_range) }}" class="btn btn-warning box-shadow btn-icon btn-rounded"><i class="fa fa-print"></i> Cetak Laporan</a>
  </div>
</div>

<section class="main-content">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        Laporan Pengiriman
        <div class="pull-right">
          <form method="get" action="">
            <label for="date_range" class="pull-right">Filter Tanggal</label>
            <div class="input-group">
              <input type="text" name="date_range" id="date_range" class="form-control pull-right datepicker" style="min-width: 100px;" value="">
              <div class="input-group-append">
                <button type="submit" class="btn btn-primary">Filter</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="card-body">
        <table id="datatable" class="table table-striped dt-responsive wrap">
            <thead>
                <tr>
                    <th width="50">No</th>
                    <th>Code Pengiriman</th>
                    <th>Code Penjualan</th>
                    <th>Pelanggan</th>
                    <th>No. HP</th>
                    <th>Alamat Tujuan</th>
                    <th>Tanggal Pengiriman</th>
                    <th>Status Pengiriman</th>
                    <th width="100">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->pengiriman ? $item->pengiriman->code : '-' }}</td>
                        <td>{{ $item->penjualan ? $item->penjualan->code : '-' }}</td>
                        <td>{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->name : '-' }}</td>
                        <td>{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->phone : '-' }}</td>
                        <td>{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->address : '-' }}</td>
                        <td>{{ date('Y-m-d', strtotime($item->pengiriman->date)) }}</td>
                        <td>{{ $item->is_delivered == "1" ? "Terkirim" : "Proses" }}</td>
                        <td>
                            <a href="{{ url('transaksi/pengiriman/'.$item->id) }}" class="btn btn-primary">Detail</a>
                        </td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('#datatable').dataTable({
        "order": [[ 0, "desc" ]]
    });

    $('.datepicker').daterangepicker({
      locale: {
        format: 'YYYY/MM/DD'
      }
    });
});

@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection