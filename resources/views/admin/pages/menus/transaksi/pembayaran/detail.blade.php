@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Pembayaran Detail</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('transaksi/pembayaran') }}">Transaksi Pembayaran</a></li>
      <li class="breadcrumb-item active">{{ $pembayaran->code }}</li>		
    </ol>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">

        <div class="card-header card-default">
            Detail Data
        </div>

        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post" class="form-horizontal" id="form-utama" action="">
            {{ csrf_field() }}
              
            <div class="row">
                
                <div class="col-md-3">
                    <h5>Tanggal Pembayaran</h5>
                    <div class="form-group">
                        <div class="input-group m-b">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" name="date" class="datepicker" value="{{ date('Y-m-d', strtotime($pembayaran->date)) }}" required disabled />
                        </div>
                    </div>
                </div>
                  
                <div class="col-md-3">
                    @if ($pembayaran->penjualan_id)
                    <h5>Transaksi Penjualan</h5>
                    <div class="input-group">
                        <select name="penjualan_id" id="penjualan_id" class="form-control" required disabled>
                            <option value="">Pilih Transaksi</option>
                            @foreach($penjualan as $item)
                                @if ($pembayaran->penjualan_id == $item->id)
                                    <option value="{{ $item->id }}" selected>{{ $item->code }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->code }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if (auth()->user()->role == 'admin')
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#penjualan-modal">Cari</button>
                        </div>
                        @endif
                    </div>
                    @endif
                    @if ($pembayaran->pembelian_id)
                    <h5>Transaksi Pembelian</h5>
                    <div class="input-group">
                        <select name="pembelian_id" id="pembelian_id" class="form-control" required disabled>
                            <option value="">Pilih Transaksi</option>
                            @foreach($pembelian as $item)
                                @if ($pembayaran->pembelian_id == $item->id)
                                    <option value="{{ $item->id }}" selected>{{ $item->code }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->code }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if (auth()->user()->role == 'admin')
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#pembelian-modal">Cari</button>
                        </div>
                        @endif
                    </div>
                    @endif
                </div>
                
                <div class="col-md-3">
                    <h5>Sudah Bayar</h5>
                    <div class="form-group">
                        <input type="text" class="form-control" name="sudah_bayar" value="{{ currencyFormat($pembayaran->pembelian->sudah_bayar) }}" disabled />
                    </div>
                </div>
                
                <div class="col-md-3">
                    <h5>Sisa Bayar</h5>
                    <div class="form-group">
                        <input type="text" class="form-control" name="sisa_bayar" value="{{ currencyFormat($pembayaran->pembelian->sisa_bayar) }}" disabled />
                    </div>
                </div>
            </div>

              
            <div class="row">
                <div class="col-md-3">
                    <h5>Ingin Bayar</h5>
                    <div class="form-group">
                        <input type="number" class="form-control" name="amount_pay" value="0" min="0" max="{{ $pembayaran->pembelian->sisa_bayar }}" required />
                    </div>
                </div>
                
                <div class="col-md-3">
                    <h5>Kekurangan Masih</h5>
                    <div class="form-group">
                        <input type="text" class="form-control" name="kekurangan_bayar" value="{{ currencyFormat($pembayaran->pembelian->sisa_bayar) }}" disabled />
                    </div>
                </div>
            </div>

            <div class="pull-right">
              <h3>Total Tagihan: <span id="price_total">{{ currencyFormat($pembayaran->amount_total) }}</span></h3>
            </div>
            <br>
            <br>
            <div class="text-right">
              <a href="{{ url('transaksi/pembayaran/print/'.$pembayaran->id) }}" class="btn btn-warning">Cetak</a>
              @if (auth()->user()->role == 'admin')
                <button type="submit" class="btn btn-danger">Update</button>
              @endif
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Transaksi Pembelian Modal -->
<div class="modal fade" id="pembelian-modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <h3 class="modal-title">Daftar Transaksi Pembelian</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <table class="table table-striped dt-responsive wrap datatable">
              <thead>
                  <tr>
                      <th width="50">ID</th>
                      <th>Kode</th>
                      <th>Sudah Bayar</th>
                      <th>Sisa Bayar</th>
                      <th>Total Tagihan</th>
                      <th>Status</th>
                      <th width="100">Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach($pembelian as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->code }}</td>
                          <td>{{ currencyFormat($item->sudah_bayar) }}</td>
                          <td>{{ currencyFormat($item->sisa_bayar) }}</td>
                          <td>{{ currencyFormat($item->price_total) }}</td>
                          <td>{{ $item->status_tagihan }}</td>
                          <td>
                              @if ($item->status_tagihan != "Lunas")
                              <a href="#" class="btn btn-primary select-pembelian" data-target="{{ $item->id }}">Pilih</a>
                              @endif
                          </td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          </div>
      </div>
  </div>
</div>
@endsection

@section('js')
<script>
function numberWithCurrency(x) {
  return "Rp " + x
    .toString()
    .replace(/\./g, ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",-";
}

$(document).ready(function(){
  $('.datatable').dataTable({
      "order": [[ 0, "desc" ]]
  });

  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $('body').on('click', '.select-pembelian', function(){
    var id = $(this).data('target');
    $('#pembelian_id').val(id);
    $('#pembelian-modal').modal('hide');

    return false;
  });

  function addItemDetail() {
    var countRow = $('.table-detail tbody tr').length;

    $.ajax({
      url: "{{ url('ajax/produk/item-html') }}",
      method: "GET",
      data: "countrow=" + countRow,
      success: function(response) {
        $('.table-detail tbody').append(response);
      }
    });
  }

  var countRow = $('.table-detail tbody tr').length;
  if (countRow <= 0) {
    addItemDetail();
  }
  $('body').on('click', '.add-item', function(){
    addItemDetail();

    return false;
  });
  $('body').on('click', '.remove-item', function(){
    $(this).parents('tr').remove();

    return false;
  });

  var produkTargetRow = "";

  $('body').on('click', '.open-produk-modal', function () {
    produkTargetRow = $(this).data('target');
    $('#produk-modal').modal('show');
  });

  function loadPriceTotal() {
    var price_total = 0;
    $( ".table-detail tbody tr" ).each(function( index ) {
      price_total += parseInt($(this).children('td').eq(6).html().replace("Rp ", "").replace(",-", "").replace(/\./g, ""));
    });

    $('#price_total').html(numberWithCurrency(price_total));
  }

  $('body').on('click', '.select-produk', function(){
    var id = $(this).data('target');
    var kategori = $(this).parents('tr').children('td').eq(3).html();
    var harga = $(this).parents('tr').children('td').eq(4).html();
    var stok = $(this).parents('tr').children('td').eq(5).html();
    var satuan = $(this).parents('tr').children('td').eq(6).html();
    var qty = $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").val();
    $('.table-detail tbody tr').eq(produkTargetRow).find("select[name='produk_id[]']").val(id);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(1).html(kategori);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(2).find("input").val(harga);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(2).find("input").attr("min", harga);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(3).html(stok);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(4).html(satuan);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").focus();
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(6).html(numberWithCurrency(qty * harga));
    $('#produk-modal').modal('hide');
    loadPriceTotal();

    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="produk_price[]"]', function(){
    var harga = parseInt($(this).val());
    var hargaMinimum = parseInt($(this).attr('min'));
    var qty = $(this).parents('tr').children('td').eq(5).find('input').val();

    if (harga < hargaMinimum) {
      $(this).val(hargaMinimum);
    }else {
      $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
      loadPriceTotal();
    }
    
    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="qty[]"]', function(){
    var harga = $(this).parents('tr').children('td').eq(2).find('input').val();
    var qty = $(this).val();
    $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
    loadPriceTotal();
    
    return false;
  });
});
@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection