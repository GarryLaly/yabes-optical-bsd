@extends('admin.master')

@section('content')
<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Pembayaran</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item active">Transaksi Pembayaran</li>		
    </ol>
  </div>
  <!-- <div class="col-lg-6 align-self-center text-right">
    <a href="{{ url('transaksi/pembayaran/create') }}" class="btn btn-success box-shadow btn-icon btn-rounded"><i class="fa fa-plus"></i> Bayar Banyak Transaksi</a>
  </div> -->
</div>

<section class="main-content">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        Transaksi Pembayaran
      </div>
      <div class="card-body">
        <table id="datatable" class="table table-striped dt-responsive wrap">
            <thead>
                <tr>
                    <th width="50">No</th>
                    <th>Code</th>
                    <th>Transaksi</th>
                    <th>Tanggal Bayar</th>
                    <th>Bayar</th>
                    <th>Sisa</th>
                    <th>Total Tagihan</th>
                </tr>
            </thead>

            <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->code }}</td>
                        <td>
                          <a href="{{ $item->penjualan_id ? url('transaksi/penjualan/'.$item->penjualan_id) : url('transaksi/pembelian/'.$item->pembelian_id) }}">{{ $item->penjualan_id ? $item->penjualan->code : $item->pembelian->code }}</a>
                        </td>
                        <td>{{ date('Y-m-d', strtotime($item->date)) }}</td>
                        <td>{{ currencyFormat($item->amount_pay) }}</td>
                        <td>{{ currencyFormat($item->amount_left) }}</td>
                        <td>{{ currencyFormat($item->amount_total) }}</td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('#datatable').dataTable({
        "order": [[ 0, "desc" ]]
    });
});

@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection