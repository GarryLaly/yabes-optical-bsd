<br>
<h5>Item Produk Retur Jual</h5>
<table class="table table-striped dt-responsive wrap table-detail">
    <thead>
      <tr>
        <th width="350">Nama Produk</th>
        <th>Kategori</th>
        <th>Harga</th>
        <th>Satuan</th>
        <th>Qty</th>
        <th width="150">Qty Retur</th>
      </tr>
    </thead>

    <tbody>
      @foreach ($penjualan_detail as $item)
      <tr>
          <td>{{ $item->produk->name }}</td>
          <td>{{ $item->produk->kategori ? $item->produk->kategori->name : '-' }}</td>
          <td>{{ $item->produk_price }}</td>
          <td>{{ $item->produk->satuan ? $item->produk->satuan->name : '-' }}</td>
          <td>{{ $item->qty }}</td>
          <td>
              <input type="hidden" class="form-control" name="produk_id[]" value="{{ $item->produk_id }}" />
              <input type="number" class="form-control" name="qty[]" value="0" min="0" max="{{ $item->qty }}" />
          </td>
      </tr>
      @endforeach`
    </tbody>
</table>