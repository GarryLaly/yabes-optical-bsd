@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Retur Jual Detail</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('transaksi/returjual') }}">Transaksi Retur Jual</a></li>
      <li class="breadcrumb-item active">{{ $returjual->code }}</li>		
    </ol>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">

        <div class="card-header card-default">
            Detail Data
        </div>

        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post" class="form-horizontal" id="form-utama" action="">
            {{ csrf_field() }}
              
            <div class="row">
                
                <div class="col-md-2">
                    <h5>Tanggal Pemesanan</h5>
                    <div class="form-group">
                        <div class="input-group m-b">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" name="date" class="datepicker" value="{{ date('Y-m-d', strtotime($returjual->date)) }}" required disabled />
                        </div>
                    </div>
                </div>
                  
                <div class="col-md-4">
                    <h5>Data Penjualan</h5>
                    <div class="input-group">
                        <select name="penjualan_id" id="penjualan_id" class="form-control" required disabled>
                            <option value="">Pilih Penjualan</option>
                            @foreach($penjualan as $item)
                                @if ($returjual->penjualan_id == $item->id)
                                    <option value="{{ $item->id }}" selected>{{ $item->name }} ({{ $item->code }})</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->code }})</option>
                                @endif
                            @endforeach
                        </select>
                        @if (auth()->user()->role == 'admin')
                        <!-- <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#penjualan-modal">Cari</button>
                        </div> -->
                        @endif
                    </div>
                </div>
            </div>

            <br>
            <h5>Item Produk Retur Jual</h5>
            <table class="table table-striped dt-responsive wrap table-detail">
                <thead>
                  <tr>
                    <th width="350">Nama Produk</th>
                    <th>Kategori</th>
                    <th>Harga</th>
                    <th>Satuan</th>
                    <th width="150">Qty Retur</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $countrow = 0;?>
                  @foreach($returjual->detail as $itemDetail)
                    <tr>
                        <td>
                            <div class="input-group">
                            <select name="produk_id[]" class="form-control" required readonly>
                                <option value="">Pilih Produk</option>
                                @foreach($produk as $item)
                                    @if ($item->id == $itemDetail['produk_id'])
                                    <option value="{{ $item->id }}" selected>{{ $item->name }} ({{ $item->code }})</option>
                                    @else
                                    <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->code }})</option>
                                    @endif
                                @endforeach
                                </select>
                                @if (auth()->user()->role == 'admin')
                                <!-- <div class="input-group-append">
                                    <button class="btn btn-outline-secondary open-produk-modal" type="button" data-target="{{ $countrow }}">Cari</button>
                                </div> -->
                                @endif
                            </div>
                        </td>
                        <td>{{ $itemDetail['produk']['kategori']['name'] }}</td>
                        <td>{{ currencyFormat($itemDetail['produk_price']) }}</td>
                        <td>{{ $itemDetail['produk']['satuan']['name'] }}</td>
                        <td>{{ $itemDetail['qty'] }}</td>
                        @if (auth()->user()->role == 'admin')
                        <!-- <td>
                            <a href="#" class="btn btn-danger remove-item">Hapus</a>
                        </td> -->
                        @endif
                    </tr>
                    <?php $countrow++;?>
                  @endforeach
                </tbody>
            </table>
            @if (auth()->user()->role == 'admin')
            <!-- <a href="#" class="btn btn-success add-item">Tambah Item</a> -->
            @endif
            <br>
            <div class="text-right">
              <a href="{{ url('transaksi/returjual/print/'.$returjual->id) }}" class="btn btn-warning">Cetak</a>
              @if (auth()->user()->role == 'admin')
                <!-- <button type="submit" class="btn btn-danger">Update</button> -->
              @endif
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js')
<script>
function numberWithCurrency(x) {
  return "Rp " + x
    .toString()
    .replace(/\./g, ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",-";
}

$(document).ready(function(){
  $('.datatable').dataTable({
      "order": [[ 0, "desc" ]]
  });

  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $('body').on('click', '.select-pelanggan', function(){
    var id = $(this).data('target');
    $('#pelanggan_id').val(id);
    $('#pelanggan-modal').modal('hide');

    return false;
  });

  function addItemDetail() {
    var countRow = $('.table-detail tbody tr').length;

    $.ajax({
      url: "{{ url('ajax/produk/item-html') }}",
      method: "GET",
      data: "countrow=" + countRow + "&price_custom=false",
      success: function(response) {
        $('.table-detail tbody').append(response);
      }
    });
  }

  var countRow = $('.table-detail tbody tr').length;
  if (countRow <= 0) {
    addItemDetail();
  }
  $('body').on('click', '.add-item', function(){
    addItemDetail();

    return false;
  });
  $('body').on('click', '.remove-item', function(){
    $(this).parents('tr').remove();

    return false;
  });

  var produkTargetRow = "";

  $('body').on('click', '.open-produk-modal', function () {
    produkTargetRow = $(this).data('target');
    $('#produk-modal').modal('show');
  });

  function loadPriceTotal() {
    var price_total = 0;
    $( ".table-detail tbody tr" ).each(function( index ) {
      price_total += parseInt($(this).children('td').eq(6).html().replace("Rp ", "").replace(",-", "").replace(/\./g, ""));
    });

    $('#price_total').html(numberWithCurrency(price_total));
  }

  $('body').on('click', '.select-produk', function(){
    var id = $(this).data('target');
    var kategori = $(this).parents('tr').children('td').eq(3).html();
    var harga = $(this).parents('tr').children('td').eq(4).html();
    var stok = $(this).parents('tr').children('td').eq(5).html();
    var satuan = $(this).parents('tr').children('td').eq(6).html();
    var qty = $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").val();
    $('.table-detail tbody tr').eq(produkTargetRow).find("select[name='produk_id[]']").val(id);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(1).html(kategori);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(2).html(numberWithCurrency(harga));
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(3).html(stok);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(4).html(satuan);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").focus();
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(6).html(numberWithCurrency(qty * harga));
    $('#produk-modal').modal('hide');
    loadPriceTotal();

    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="produk_price[]"]', function(){
    var harga = parseInt($(this).val());
    var hargaMinimum = parseInt($(this).attr('min'));
    var qty = $(this).parents('tr').children('td').eq(5).find('input').val();

    if (harga < hargaMinimum) {
      $(this).val(hargaMinimum);
    }else {
      $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
      loadPriceTotal();
    }
    
    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="qty[]"]', function(){
    var harga = $(this).parents('tr').children('td').eq(2).html().replace("Rp ", "").replace(",-", "").replace(/\./g, "");
    var qty = $(this).val();
    $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
    loadPriceTotal();
    
    return false;
  });
});
@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection