<tr>
    <td>
        <div class="input-group">
        <select name="penjualan_id[]" class="form-control" required readonly>
            <option value="">Pilih Penjualan</option>
            @foreach($data as $item)
            <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->code }})</option>
            @endforeach
        </select>
        <div class="input-group-append">
            <button class="btn btn-outline-secondary open-penjualan-modal" type="button" data-target="{{ $countrow }}">Cari</button>
        </div>
        </div>
    </td>
    <td>-</td>
    <td>-</td>
    <td>-</td>
    <td>-</td>
    @if ($status_pengiriman == 'true')
    <td>-</td>
    @endif
    <td>
        <a href="#" class="btn btn-danger remove-item">Hapus</a>
    </td>
</tr>