@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Pengiriman Baru</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('transaksi/pengiriman') }}">Transaksi Pengiriman</a></li>
      <li class="breadcrumb-item active">Baru</li>		
    </ol>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">

        <div class="card-header card-default">
            Buat Data Baru
        </div>

        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post" class="form-horizontal" id="form-utama" action="">
            {{ csrf_field() }}
              
            <div class="row">
                
              <div class="col-md-2">
                <h5>Tanggal Pengiriman</h5>
                <div class="form-group">
                  <div class="input-group m-b">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                    <input type="text" name="date" class="datepicker" required />
                  </div>
                </div>
              </div>
                       
            </div>

            <br>
            <h5>Item Penjualan (yang akan dikirim)</h5>
            <table class="table table-striped dt-responsive wrap table-detail">
                <thead>
                  <tr>
                    <th width="350">Penjualan</th>
                    <th>Pelanggan</th>
                    <th>Tanggal</th>
                    <th>Total Harga</th>
                    <th>Status Tagihan</th>
                    <th width="100">Action</th>
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
            <a href="#" class="btn btn-success add-item">Tambah Item</a>
            <div class="pull-right">
              <h3>Total Tagihan: <span id="price_total">{{ currencyFormat(0) }}</span></h3>
            </div>
            <br>
            <br>
            <br>
            <br>
            <div class="text-right">
              <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Penjualan Modal -->
<div class="modal fade" id="penjualan-modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <h3 class="modal-title">Daftar Penjualan</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
          <table class="table table-striped dt-responsive wrap datatable">
              <thead>
                  <tr>
                      <th width="50">No</th>
                      <th>Code</th>
                      <th>Pelanggan</th>
                      <th>Tanggal</th>
                      <th>Total Harga</th>
                      <th>Status Tagihan</th>
                      <th width="100">Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach($penjualan as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->code }}</td>
                          <td>{{ $item->pelanggan ? $item->pelanggan->name : '-' }}</td>
                          <td>{{ date('Y-m-d', strtotime($item->date)) }}</td>
                          <td>{{ currencyFormat($item->price_total) }}</td>
                          <td>{{ $item->status_tagihan }}</td>
                          <td>
                              <a href="#" class="btn btn-primary select-penjualan" data-target="{{ $item->id }}">Pilih</a>
                          </td>
                      </tr>
                  @endforeach
                  
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          </div>
      </div>
  </div>
</div>
@endsection

@section('js')
<script>
function numberWithCurrency(x) {
  return "Rp " + x
    .toString()
    .replace(/\./g, ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",-";
}

$(document).ready(function(){
  $('.datatable').dataTable({
      "order": [[ 0, "desc" ]]
  });

  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $('body').on('click', '.select-supplier', function(){
    var id = $(this).data('target');
    $('#suplier_id').val(id);
    $('#supplier-modal').modal('hide');

    return false;
  });

  $('body').on('change', '#pembayaran', function(){
    if ($(this).val() == "Angsur") {
      $("#amount_pay").val(0);
      $("#amount_pay_total").html(numberWithCurrency(0));
      $("#amount_left_total").parents('h3').show();
      $("#amount_pay").removeAttr('readonly');
    }else {
      $("#amount_left_total").parents('h3').hide();
      $("#amount_pay").attr('readonly', true);
      loadPriceTotal();
    }
  });

  $('body').on('change', '#amount_pay', function(){
    var amountPay = $(this).val();
    var amountPayMax = parseInt($(this).attr('max'));
    var amountLeft = 0
    var priceTotal = $('#price_total').html().replace("Rp ", "").replace(",-", "").replace(/\./g, "");

    if (amountPay > amountPayMax) {
      $(this).val(amountPayMax);
    }else {
      amountLeft = parseInt(priceTotal) - parseInt(amountPay);
      $("#amount_pay_total").html(numberWithCurrency(amountPay));
    }

    $('#amount_left_total').html(numberWithCurrency(amountLeft));
  });

  function addItemDetail() {
    var countRow = $('.table-detail tbody tr').length;

    $.ajax({
      url: "{{ url('ajax/penjualan/item-html') }}",
      method: "GET",
      data: "countrow=" + countRow + "&status_pengiriman=false",
      success: function(response) {
        $('.table-detail tbody').append(response);
      }
    });
  }

  var countRow = $('.table-detail tbody tr').length;
  if (countRow <= 0) {
    addItemDetail();
  }
  $('body').on('click', '.add-item', function(){
    addItemDetail();

    return false;
  });
  $('body').on('click', '.remove-item', function(){
    $(this).parents('tr').remove();

    return false;
  });

  var penjualanTargetRow = "";

  $('body').on('click', '.open-penjualan-modal', function () {
    penjualanTargetRow = $(this).data('target');
    $('#penjualan-modal').modal('show');
  });

  function loadPriceTotal() {
    var price_total = 0;
    $( ".table-detail tbody tr" ).each(function( index ) {
      price_total += parseInt($(this).children('td').eq(3).html().replace("Rp ", "").replace(",-", "").replace(/\./g, ""));
    });

    if (price_total > 0) {
      $('#price_total').html(numberWithCurrency(price_total));
    }
  }

  $('body').on('click', '.select-penjualan', function(){
    var id = $(this).data('target');
    var pelanggan = $(this).parents('tr').children('td').eq(2).html();
    var tanggal = $(this).parents('tr').children('td').eq(3).html();
    var total_harga = $(this).parents('tr').children('td').eq(4).html();
    var status_tagihan = $(this).parents('tr').children('td').eq(5).html();
    var qty = $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(5).find("input").val();
    $('.table-detail tbody tr').eq(penjualanTargetRow).find("select[name='penjualan_id[]']").val(id);
    $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(1).html(pelanggan);
    $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(2).html(tanggal);
    $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(3).html(total_harga);
    $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(4).html(status_tagihan);
    $('#penjualan-modal').modal('hide');
    loadPriceTotal();

    return false;
  });
});
@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection