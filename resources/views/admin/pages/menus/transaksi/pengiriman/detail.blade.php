@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Pengiriman Detail</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('transaksi/pengiriman') }}">Transaksi Pengiriman</a></li>
      <li class="breadcrumb-item active">{{ $pengiriman->code }}</li>		
    </ol>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">

        <div class="card-header card-default">
            Detail Data
        </div>

        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post" class="form-horizontal" id="form-utama" action="">
            {{ csrf_field() }}
              
            <div class="row">
                
                <div class="col-md-2">
                    <h5>Tanggal Pemesanan</h5>
                    <div class="form-group">
                        <div class="input-group m-b">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" name="date" class="datepicker" value="{{ date('Y-m-d', strtotime($pengiriman->date)) }}" required disabled />
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <h5>Item Penjualan (yang akan dikirim)</h5>
            <table class="table table-striped dt-responsive wrap table-detail">
                <thead>
                  <tr>
                    <th width="350">Penjualan</th>
                    <th>Pelanggan</th>
                    <th>Tanggal</th>
                    <th>Total Harga</th>
                    <th>Status Tagihan</th>
                    <th>Status Pengiriman</th>
                    <!-- <th width="300">Action</th> -->
                  </tr>
                </thead>

                <tbody>
                  <?php $countrow = 0;?>
                  @foreach($pengiriman->detail as $itemDetail)
                    <tr>
                        <td>
                            <div class="input-group">
                            <select name="penjualan_id[]" class="form-control" required readonly>
                                <option value="">Pilih Penjualan</option>
                                @foreach($penjualan as $item)
                                    @if ($item->id == $itemDetail['penjualan_id'])
                                    <option value="{{ $item->id }}" selected>{{ $item->name }} ({{ $item->code }})</option>
                                    @else
                                    <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->code }})</option>
                                    @endif
                                @endforeach
                                </select>
                                @if (auth()->user()->role == 'admin')
                                <!-- <div class="input-group-append">
                                    <button class="btn btn-outline-secondary open-penjualan-modal" type="button" data-target="{{ $countrow }}">Cari</button>
                                </div> -->
                                @endif
                            </div>
                        </td>
                        <td>{{ $itemDetail['penjualan']['pelanggan']['name'] }}</td>
                        <td>{{ date('Y-m-d', strtotime($itemDetail['penjualan']['date'])) }}</td>
                        <td>{{ currencyFormat($itemDetail['penjualan']['price_total']) }}</td>
                        <td>{{ $itemDetail['penjualan']['status_tagihan'] }}</td>
                        <td>{{ $itemDetail['is_delivered'] == "1" ? "Terkirim" : "Proses" }}</td>
                        @if ($itemDetail['is_delivered'] == "0")
                        <td>
                            <a href="#" class="btn btn-success terkirim-post" data-pengiriman="{{ $itemDetail['id'] }}" data-penjualan="{{ $itemDetail['penjualan_id'] }}">Sudah Sampai</a>
                            @if (auth()->user()->role == 'admin')
                            <!-- <a href="#" class="btn btn-danger remove-item">Hapus</a> -->
                            @endif
                        </td>
                        @endif
                    </tr>
                    <?php $countrow++;?>
                  @endforeach
                </tbody>
            </table>
            @if (auth()->user()->role == 'admin')
            <!-- <a href="#" class="btn btn-success add-item">Tambah Item</a> -->
            @endif
            <br>
            <br>
            <br>
            <div class="text-right">
              <a href="{{ url('transaksi/pengiriman/print/'.$pengiriman->id) }}" class="btn btn-warning">Cetak</a>
              @if (auth()->user()->role == 'admin')
                <!-- <button type="submit" class="btn btn-danger">Update</button> -->
              @endif
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Penjualan Modal -->
<div class="modal fade" id="penjualan-modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <h3 class="modal-title">Daftar Penjualan</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
          <table class="table table-striped dt-responsive wrap datatable">
              <thead>
                  <tr>
                      <th width="50">No</th>
                      <th>Code</th>
                      <th>Pelanggan</th>
                      <th>Tanggal</th>
                      <th>Total Harga</th>
                      <th>Status Tagihan</th>
                      <th width="100">Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach($penjualan as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->code }}</td>
                          <td>{{ $item->pelanggan ? $item->pelanggan->name : '-' }}</td>
                          <td>{{ date('Y-m-d', strtotime($item->date)) }}</td>
                          <td>{{ currencyFormat($item->price_total) }}</td>
                          <td>{{ $item->status_tagihan }}</td>
                          <td>
                              <a href="#" class="btn btn-primary select-penjualan" data-target="{{ $item->id }}">Pilih</a>
                          </td>
                      </tr>
                  @endforeach
                  
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          </div>
      </div>
  </div>
</div>
@endsection

@section('js')
<script>
function numberWithCurrency(x) {
  return "Rp " + x
    .toString()
    .replace(/\./g, ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",-";
}

$(document).ready(function(){
  $('.datatable').dataTable({
      "order": [[ 0, "desc" ]]
  });

  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  function addItemDetail() {
    var countRow = $('.table-detail tbody tr').length;

    $.ajax({
      url: "{{ url('ajax/penjualan/item-html') }}",
      method: "GET",
      data: "countrow=" + countRow + "&status_pengiriman=true",
      success: function(response) {
        $('.table-detail tbody').append(response);
      }
    });
  }

  var countRow = $('.table-detail tbody tr').length;
  if (countRow <= 0) {
    addItemDetail();
  }
  $('body').on('click', '.add-item', function(){
    addItemDetail();

    return false;
  });
  $('body').on('click', '.remove-item', function(){
    $(this).parents('tr').remove();

    return false;
  });

  $('body').on('click', '.terkirim-post', function () {
    var pengirimanID = $(this).data('pengiriman');
    var penjualanID = $(this).data('penjualan');

    $.ajax({
      url: "{{ url('transaksi/pengiriman/'.$pengiriman->id.'/terkirim') }}",
      method: "POST",
      data: "penjualan_id=" + penjualanID,
      success: function(response) {
        location.reload();
      }
    });
    
    return false;
  });

  var penjualanTargetRow = "";

  $('body').on('click', '.open-penjualan-modal', function () {
    penjualanTargetRow = $(this).data('target');
    $('#penjualan-modal').modal('show');
  });

  function loadPriceTotal() {
    var price_total = 0;
    $( ".table-detail tbody tr" ).each(function( index ) {
      price_total += parseInt($(this).children('td').eq(3).html().replace("Rp ", "").replace(",-", "").replace(/\./g, ""));
    });

    if (price_total > 0) {
      $('#price_total').html(numberWithCurrency(price_total));
    }
  }

  $('body').on('click', '.select-penjualan', function(){
    var id = $(this).data('target');
    var pelanggan = $(this).parents('tr').children('td').eq(2).html();
    var tanggal = $(this).parents('tr').children('td').eq(3).html();
    var total_harga = $(this).parents('tr').children('td').eq(4).html();
    var status_tagihan = $(this).parents('tr').children('td').eq(5).html();
    var qty = $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(5).find("input").val();
    $('.table-detail tbody tr').eq(penjualanTargetRow).find("select[name='penjualan_id[]']").val(id);
    $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(1).html(pelanggan);
    $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(2).html(tanggal);
    $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(3).html(total_harga);
    $('.table-detail tbody tr').eq(penjualanTargetRow).children("td").eq(4).html(status_tagihan);
    $('#penjualan-modal').modal('hide');
    loadPriceTotal();

    return false;
  });
});
@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection