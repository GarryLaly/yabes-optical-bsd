@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Penerimaan Baru</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('transaksi/penerimaan') }}">Transaksi Penerimaan</a></li>
      <li class="breadcrumb-item active">Baru</li>		
    </ol>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">

        <div class="card-header card-default">
            Buat Data Baru
        </div>

        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post" class="form-horizontal" id="form-utama" action="">
            {{ csrf_field() }}
              
            <div class="row">
                
              <div class="col-md-2">
                <h5>Tanggal Retur</h5>
                <div class="form-group">
                  <div class="input-group m-b">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                    <input type="text" name="date" class="datepicker" required />
                  </div>
                </div>
              </div>
                  
              <div class="col-md-4">
                <h5>Data Pembelian</h5>
                <div class="input-group">
                  <select name="pembelian_id" id="pembelian_id" class="form-control" required readonly>
                    <option value="">Pilih Pembelian</option>
                    @foreach($pembelian as $item)
                      <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->code }})</option>
                    @endforeach
                  </select>
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#pembelian-modal">Cari</button>
                  </div>
                </div>
              </div>
                       
            </div>

            <div class="pembelian-detail"></div>
            <!-- <a href="#" class="btn btn-success add-item">Tambah Item</a> -->
            <br>
            <div class="text-right">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Pembelian Modal -->
<div class="modal fade" id="pembelian-modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <h3 class="modal-title">Daftar Pembelian</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <table class="table table-striped dt-responsive wrap datatable">
              <thead>
                  <tr>
                      <th width="50">ID</th>
                      <th>Kode</th>
                      <th>Nama Pelanggan</th>
                      <th>Tanggal Jual</th>
                      <th>Harga Total</th>
                      <th width="100">Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach($pembelian as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->code }}</td>
                          <td>{{ $item->pelanggan ? $item->pelanggan->name : '-' }}</td>
                          <td>{{ date('Y-m-d', strtotime($item->date)) }}</td>
                          <td>{{ currencyFormat($item->price_total) }}</td>
                          <td>
                              <a href="#" class="btn btn-primary select-pembelian" data-target="{{ $item->id }}">Pilih</a>
                          </td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          </div>
      </div>
  </div>
</div>
@endsection

@section('js')
<script>
function numberWithCurrency(x) {
  return "Rp " + x
    .toString()
    .replace(/\./g, ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",-";
}

$(document).ready(function(){
  $('.datatable').dataTable({
      "order": [[ 0, "desc" ]]
  });

  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $('body').on('click', '.select-pembelian', function(){

    return false;
  });

  $('body').on('click', '.select-pembelian', function(){
    var id = $(this).data('target');
    $('#pembelian_id').val(id);
    $('#pembelian-modal').modal('hide');

    $.ajax({
      url: "{{ url('transaksi/penerimaan/pembelian-detail') }}" + '/' + id,
      method: "GET",
      success: function(response) {
        $('.pembelian-detail').html(response);
      }
    });

    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="qty[]"]', function(){
    var qty = parseInt($(this).val());
    var qtyMax = parseInt($(this).attr('max'));

    if (qty > qtyMax) {
      $(this).val(qtyMax);
    }else {
      $(this).val(qty);
    }
    
    return false;
  });
});
@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection