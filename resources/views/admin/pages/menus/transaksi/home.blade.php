@extends('admin.master')

@section('content')
<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Halaman Transaksi</h2>
  </div>
</div>

<section class="main-content">
<div class="row">
  <div class="col-md-12">
      <div class="row">
          <div class="col-md-3">
            <div class="widget widget-chart white-bg padding-0">
                <div class="widget-title">
                  <span class="label label-primary float-right">Ke Halaman</span>
                  <h2 class="margin-b-0">Penjualan</h2>
                </div>
                <div class="widget-content">
                  <p class="text-muted margin-b-0">Total Penjualan</p>
                  <h1 class="margin-b-10  text-primary">79,2458</h1>
                </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="widget widget-chart white-bg padding-0">
                
                <div class="widget-title">
                  <span class="label label-success float-right">Monthly</span>
                  <h2 class="margin-b-0">Pembelian</h2>
                </div>

                <div class="widget-content">
                  <a href="{{ url('transaksi/pembelian') }}" class="btn btn-success btn-border box-shadow btn-square pull-right">Ke Halaman</i></a>
                  <p class="text-muted margin-b-0">Total Pembelian</p>
                  <h1 class="margin-b-10  text-primary">79,2458</h1>                            
                </div>
            
            </div>
          </div>

          <div class="col-md-3">
            <div class="widget widget-chart white-bg padding-0">
                <div class="widget-title">
                  <span class="label label-warning float-right">Ke Halaman</span>
                  <h2 class="margin-b-0">Retur Jual</h2>
                </div>
                <div class="widget-content">
                  <p class="text-muted margin-b-0">Total Retur Jual</p>
                  <h1 class="margin-b-10  text-primary">79,2458</h1>
                </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="widget widget-chart white-bg padding-0">
                <div class="widget-title">
                  <span class="label label-danger float-right">Ke Halaman</span>
                  <h2 class="margin-b-0">Pembayaran</h2>
                </div>
                <div class="widget-content">
                  <p class="text-muted margin-b-0">Total Pembayaran</p>
                  <h1 class="margin-b-10  text-primary">79,2458</h1>                            
                </div>
            </div>
          </div>
                    
      </div>
      <div class="row">
          <div class="col-md-3">
            <div class="widget widget-chart white-bg padding-0">
                <div class="widget-title">
                  <span class="label label-primary float-right">Ke Halaman</span>
                  <h2 class="margin-b-0">Pengiriman</h2>
                </div>
                <div class="widget-content">
                  <p class="text-muted margin-b-0">Total Pengiriman</p>
                  <h1 class="margin-b-10  text-primary">79,2458</h1>                            
                </div>
            </div>
          </div>
      </div>


  </div>
</div>
</section>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('#datatable').dataTable({
        "order": [[ 0, "desc" ]]
    });
});

@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
@endif
</script>
@endsection