@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Master Produk</h2>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header card-default">
            Buat Data Baru
        </div>
        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <form method="post" class="form-horizontal" action="">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="code">Kode</label>
              <input type="text" value="{{ old('code') ? old('code') : $newcode }}" class="form-control" id="code" name="code" />
            </div>
            <div class="form-group">
              <label for="name">Nama Produk</label>
              <input type="text" value="{{ old('name') }}" class="form-control" id="name" name="name" />
            </div>
            <div class="form-group">
              <label for="kategori_id">Kategori</label>
              <select name="kategori_id" id="kategori_id" class="form-control select2">
                @foreach($kategori as $item)
                  @if(old('kategori_id') == $item->id)
                    <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                  @else
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="price_buy">Harga Beli</label>
              <input type="number" value="{{ old('price_buy') }}" class="form-control" id="price_buy" name="price_buy" />
            </div>
            <div class="form-group">
              <label for="price_sell">Harga Jual</label>
              <input type="number" value="{{ old('price_sell') }}" class="form-control" id="price_sell" name="price_sell" />
            </div>
            <div class="form-group">
              <label for="stock">Stok</label>
              <input type="number" value="{{ old('stock') }}" class="form-control" id="stock" name="stock" />
            </div>
            <div class="form-group">
              <label for="satuan_id">Satuan</label>
              <select name="satuan_id" id="satuan_id" class="form-control select2">
                @foreach($satuan as $item)
                  @if(old('satuan_id') == $item->id)
                    <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                  @else
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
