@extends('admin.master')

@section('content')
<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Absensi</h2>
  </div>
  <div class="col-lg-6 align-self-center text-right">
    <a href="{{ url('absensi/create') }}" class="btn btn-success box-shadow btn-icon btn-rounded"><i class="fa fa-plus"></i> Buat Baru</a>
  </div>
</div>

<section class="main-content">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        Absensi Pegawai
      </div>
      <div class="card-body">
        <table id="datatable" class="table table-striped dt-responsive wrap">
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th>Nama Pegawai</th>
                    <th>Tanggal</th>
                    <th>Waktu Mulai Kerja</th>
                    <th>Waktu Selesai Kerja</th>
                    <th>Status</th>
                    @if (auth()->user()->role == 'admin')
                    <th width="200">Action</th>
                    @endif
                </tr>
            </thead>

            <tbody>
                @foreach($data as $item)
                    <?php
                    $status = "Menunggu";
                    if ($item->is_approved == "1") {
                      $status = "Disetujui";
                    }
                    if ($item->is_approved == "0") {
                      $status = "Ditolak";
                    }
                    ?>
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->pengguna->username }}</td>
                        <td>{{ date('Y-m-d', strtotime($item->date)) }}</td>
                        <td>{{ $item->time_start }}</td>
                        <td>{{ $item->time_end }}</td>
                        <td>{{ $status }}</td>
                        @if (auth()->user()->role == 'admin')
                        <td>
                            @if ($item->is_approved <> "0" and $item->is_approved <> "1")
                            <a href="{{ url('absensi/approve/' . $item->id) }}" onclick="return confirm('Apa anda yakin ingin menyetujui data ini?')" class="btn btn-success">Disetujui</a>
                            <a href="{{ url('absensi/reject/' . $item->id) }}" onclick="return confirm('Apa anda yakin ingin menolak data ini?')" class="btn btn-danger">Tolak</a>
                            @endif
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('#datatable').dataTable({
        "order": [[ 0, "desc" ]]
    });
});

@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection