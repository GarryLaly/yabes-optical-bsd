@extends('admin.master')

@section('content')
<div class="row page-header">
    <div class="col-lg-6 align-self-center ">
        <h2>Dashboard</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
    </div>
</div>

<section class="main-content">
    <div class="row w-no-padding margin-b-30">
    
        <div class="col-md-3">
            <div class="widget  bg-light">
                <div class="row row-table ">
                    <div class="margin-b-30">
                        <h2 class="margin-b-5">Produk</h2>
                        <p class="text-muted">Total Produk</p>
                        <span class="float-right text-primary widget-r-m">{{ $produk_total }}</span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="widget  bg-light">
                <div class="row row-table ">
                    <div class="margin-b-30">
                        <h2 class="margin-b-5">Transaksi Penjualan</h2>
                        <p class="text-muted">Total Penjualan</p>
                        <span class="float-right text-indigo widget-r-m">{{ $penjualan_total }}</span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="widget  bg-light">
                <div class="row row-table ">
                    <div class="margin-b-30">
                        <h2 class="margin-b-5">Transaksi Pengiriman</h2>
                        <p class="text-muted">Total Pengiriman</p>
                        <span class="float-right text-success widget-r-m">{{ $pembayaran_total }}</span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="widget  bg-light">
                <div class="row row-table ">
                    <div class="margin-b-30">
                        <h2 class="margin-b-5">Transaksi Pembayaran</h2>
                        <p class="text-muted">Total Pembayaran</p>
                        <span class="float-right text-warning widget-r-m">{{ $pengiriman_total }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card bg-chart ">
                <div class="card-header text-white anime">
                    Omset Bulan ini			
                </div>
                <div class="card-body">
                    <div>
                        <canvas id="myChart"  height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Kategori Terlaris Bulan Ini
                </div>
                <div class="card-body">
                    <div id="donut"></div>
                    <ul class="list-1 list-group">
                        <li class="list-group-item">Kategori A <span class="float-right text-indigo">45.0%</span></li>
                        <li class="list-group-item">Kategori B <span class="float-right text-primary">25.0%</span></li>
                        <li class="list-group-item">Kategori C <span class="float-right text-teal">15.0%</span></li>
                        <li class="list-group-item">Kategori D <span class="float-right text-muted">15.0%</span></li>                  
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Produk Terlaris Bulan Ini
                </div>
                <div class="card-body">
                    <div id="donut2"></div>
                    <ul class="list-1 list-group">
                        <li class="list-group-item">Produk A <span class="float-right text-indigo">45.0%</span></li>
                        <li class="list-group-item">Produk B <span class="float-right text-primary">25.0%</span></li>
                        <li class="list-group-item">Produk C <span class="float-right text-teal">15.0%</span></li>
                        <li class="list-group-item">Produk D <span class="float-right text-muted">15.0%</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
            <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                Pelanggan Favorit
                </div>

            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<!--Chart Script-->
<script src="{{ url('vendor/fixed-plus/lib/chartjs/chart.min.js') }}"></script>
<script src="{{ url('vendor/fixed-plus/lib/chartjs/chartjs-sass.js') }}"></script>

<!--Vetor Map Script-->
<script src="{{ url('vendor/fixed-plus/lib/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{ url('vendor/fixed-plus/lib/vectormap/jquery-jvectormap-us-aea-en.js') }}"></script>

<!-- Chart C3 -->
<script src="{{ url('vendor/fixed-plus/lib/chart-c3/d3.min.js') }}"></script>
<script src="{{ url('vendor/fixed-plus/lib/chart-c3/c3.min.js') }}"></script>

<!-- Datatables-->
<script src="{{ url('vendor/fixed-plus/lib/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('vendor/fixed-plus/lib/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('vendor/fixed-plus/lib/toast/jquery.toast.min.js') }}"></script>
<script src="{{ url('vendor/fixed-plus/js/dashboard.js') }}"></script>
@endsection