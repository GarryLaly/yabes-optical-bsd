<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Kategori;
use App\Models\Satuan;
use Response;

class ProdukController extends Controller
{
  public function index()
  {
    $data['data'] = Produk::orderBy('id', 'desc')->get();

    return view('admin.pages.menus.master.produk.table', $data);
  }

  public function itemHTML(Request $request)
  {
    $req = $request->all();

    $data['data'] = Produk::orderBy('id', 'desc')->get();
    $data['countrow'] = $req['countrow'];
    $data['price_custom'] = $req['price_custom'];

    return view('admin.pages.menus.master.produk.item', $data);
  }

  public function create()
  {
    $data['kategori'] = Kategori::all();
    $data['satuan'] = Satuan::all();
    $data['newcode'] = Produk::orderBy('id', 'desc')->first();
    if (!$data['newcode']) {
      $data['newcode'] = "PRO1";
    }else {
      $data['newcode'] = "PRO" . ($data['newcode']->id + 1);
    }

    return view('admin.pages.menus.master.produk.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
      'kategori_id' => 'required',
      'satuan_id' => 'required',
      'code' => 'required|unique:produk|max:20',
      'name' => 'required|max:30',
      'price_buy' => 'required|numeric',
      'price_sell' => 'required|numeric',
      // 'stock' => 'required|numeric',
    ]);

    $req['name'] = strtoupper($req['name']);

    $produk = Produk::create($req);

    if ($produk) {
      return redirect('/master/produk')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/produk')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function edit($id)
  {
    $data['data'] = Produk::find($id);
    $data['kategori'] = Kategori::all();
    $data['satuan'] = Satuan::all();

    return view('admin.pages.menus.master.produk.edit', $data);
  }

  public function update($id, Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
      'kategori_id' => 'required',
      'satuan_id' => 'required',
      'name' => 'required|max:30',
      // 'price_buy' => 'required|numeric',
      'price_sell' => 'required|numeric',
      // 'stock' => 'required|numeric',
    ]);
    $produk = Produk::where('id', $id)->update($req);

    if ($produk) {
      return redirect('/master/produk')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/produk')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function delete($id)
  {
    $produk = Produk::find($id);
    $produk->delete();

    if ($produk) {
      return redirect('/master/produk')->with([
        'success' => true,
        'message' => 'Berhasil menghapus data',
      ]);
    }

    return redirect('/master/produk')->with([
      'success' => false,
      'message' => 'Gagal menghapus data',
    ]);
  }
}
