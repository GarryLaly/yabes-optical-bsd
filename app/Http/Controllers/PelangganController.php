<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pelanggan;
use Response;

class PelangganController extends Controller
{
  public function index()
  {
    $data['data'] = Pelanggan::orderBy('id', 'desc')->get();

    return view('admin.pages.menus.master.pelanggan.table', $data);
  }

  public function create()
  {
    $data['newcode'] = Pelanggan::orderBy('id', 'desc')->first();
    if (!$data['newcode']) {
      $data['newcode'] = "PEL1";
    }else {
      $data['newcode'] = "PEL" . ($data['newcode']->id + 1);
    }

    return view('admin.pages.menus.master.pelanggan.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
      'code' => 'required|unique:pelanggan|max:20',
      'name' => 'required|max:30',
      'phone' => 'required|max:20',
      'address' => 'required',
    ]);

    $req['name'] = strtoupper($req['name']);
    $req['address'] = strtoupper($req['address']);

    $pelanggan = Pelanggan::create($req);

    if ($pelanggan) {
      return redirect('/master/pelanggan')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/pelanggan')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function edit($id)
  {
    $data['data'] = Pelanggan::find($id);

    return view('admin.pages.menus.master.pelanggan.edit', $data);
  }

  public function update($id, Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
      'name' => 'required|max:30',
      'phone' => 'required|max:20',
      'address' => 'required',
    ]);

    $req['name'] = strtoupper($req['name']);
    $req['address'] = strtoupper($req['address']);
    
    $pelanggan = Pelanggan::where('id', $id)->update($req);

    if ($pelanggan) {
      return redirect('/master/pelanggan')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/pelanggan')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function delete($id)
  {
    $pelanggan = Pelanggan::find($id);
    $pelanggan->delete();

    if ($pelanggan) {
      return redirect('/master/pelanggan')->with([
        'success' => true,
        'message' => 'Berhasil menghapus data',
      ]);
    }

    return redirect('/master/pelanggan')->with([
      'success' => false,
      'message' => 'Gagal menghapus data',
    ]);
  }
}
