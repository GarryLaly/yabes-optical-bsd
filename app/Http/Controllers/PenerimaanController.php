<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pembelian;
use App\Models\PembelianDetail;
use App\Models\Pelanggan;
use App\Models\Produk;
use App\Models\Penerimaan;
use App\Models\PenerimaanDetail;
use Response;

class PenerimaanController extends Controller
{
  	public function index()
  	{
  	  $data['data'] = Penerimaan::with('pembelian')->orderBy('id', 'desc')->get();
	
	  	  return view('admin.pages.menus.transaksi.penerimaan.table', $data);
  	}

  	public function create(Request $request)
    {

      $data['pembelian'] = Pembelian::doesnthave('penerimaan')->orderBy('id', 'desc')->get();
      $data['produk'] = Produk::orderBy('id', 'desc')->get();

      return view ('admin.pages.menus.transaksi.penerimaan.entry', $data);
    }

  	public function pembelianDetail($pembelianID)
    {
      $data['pembelian_detail'] = PembelianDetail::where('pembelian_id', $pembelianID)->get();

      return view ('admin.pages.menus.transaksi.penerimaan.pembelian-detail', $data);
    }

    public function StoreNew(Request $request)
    {
      $req = $request->except('_token');
      $validator = $this->validate($request, [
        'date' => 'required',
        'pembelian_id' => 'required',
      ]);
      if ($validator and $validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
      }
  
      \DB::beginTransaction();
  
      try {
        $produk_idArray = $req['produk_id'];
        $qtyArray = $req['qty'];
        $produk_priceArray = $req['produk_price'];
        $index = 0;
        $price_total = 0;
        $penerimaan_detail = array();
        foreach($produk_idArray as $produkID) {
          $produk = Produk::find($produkID);
    
          if ($produk) {
            $produk_id = $produk->id;
            $produk_name = $produk->name;
            $qty = $qtyArray[$index];
            $produk_price = $produk_priceArray[$index];
            $price_subtotal = $qty * $produk_price;
    
            if ($qty > 0) {
              $penerimaan_detail[] = array(
                'produk_id' => $produk_id,
                'produk_name' => $produk_name,
                'qty' => $qty,
                'produk_price' => $produk_price,
                'price_subtotal' => $price_subtotal,
              );
      
              // SUM for total
              $price_total += $price_subtotal;

              // average logic
              $newPriceSell = (($produk->price_buy * $produk->stock) + ($produk_price * $qty)) / ($produk->stock + $qty);
              $produk->price_buy = $newPriceSell;

              // Update stok produk
              $produk->stock = $produk->stock + $qty;
              $produk->save();
      
              $index++;
            }
          }
        }
  
        if (count($penerimaan_detail) <= 0) {
          return redirect()->back()->with([
            'success' => false,
            'message' => 'Produk harus diisi minimal 1 item',
          ])->withInput();
        }
  
        $code = Penerimaan::orderBy('code', 'desc')->first();
        if (!$code) {
          $req['code'] = "TERIMA1";
        }else {
          $code = $code->code;
          $code = str_replace("TERIMA", "", $code);
          $req['code'] = "TERIMA" . ($code + 1);
        }
        $req['pengguna_id'] = auth()->user()->id;
        $req['price_total'] = $price_total;
        $penerimaan = Penerimaan::create($req);
  
        foreach($penerimaan_detail as $item) {
          $item['penerimaan_id'] = $penerimaan->id;
          PenerimaanDetail::create($item);
        }
  
        \DB::commit();
        // all good
    
        if ($penerimaan) {
          return redirect('/transaksi/penerimaan/'.$penerimaan->id)->with([
            'success' => true,
            'message' => 'Berhasil menyimpan data',
          ]);
        }
      } catch (\Exception $e) {
        \DB::rollback();
        // something went wrong
        return redirect('/transaksi/penerimaan')->with([
          'success' => false,
          'message' => 'Gagal menyimpan data .' . $e->getMessage(),
        ]);
      }
    }
  
    public function show($id)
    {
      $data['penerimaan'] = Penerimaan::with('detail')->find($id);
      $data['pembelian'] = Pembelian::orderBy('id', 'desc')->get();
      $data['produk'] = Produk::orderBy('id', 'desc')->get();
  
      return view ('admin.pages.menus.transaksi.penerimaan.detail', $data);
    }

    public function print($id)
    {
      $data['penerimaan'] = Penerimaan::with('detail','pembelian')->find($id);
  
      $pdf = \PDF::loadView('admin.pages.menus.transaksi.penerimaan.print', $data);
      return $pdf->stream('penerimaan_'.$id.'.pdf');
    }
}
