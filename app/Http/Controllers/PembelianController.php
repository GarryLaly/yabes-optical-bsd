<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pembelian;
use App\Models\PembelianDetail;
use App\Models\Pembayaran;
use App\Models\Supplier;
use App\Models\Produk;
use Response;

class PembelianController extends Controller
{
  public function home()
  {
    return view ('admin.pages.menus.transaksi.home');
  }

  public function index()
  {
    $data['data'] = Pembelian::with('supplier')->orderBy('id', 'desc')->get();

    return view('admin.pages.menus.transaksi.pembelian.table', $data);
  }

  public function create(Request $request)
  {
    $produk_idArray = $request->old('produk_id');
    $qtyArray = $request->old('qty');
    $produk_priceArray = $request->old('produk_price');
    $index = 0;
    $price_total = 0;
    $pembelian_detail = array();
    if ($produk_idArray) {
      foreach($produk_idArray as $produkID) {
        $produk = Produk::with('kategori','satuan')->find($produkID);

        if ($produk) {
          $produk_id = $produk->id;
          $produk_name = $produk->name;
          $kategori = $produk->kategori ? $produk->kategori->name : '';
          $stok = $produk->stock;
          $satuan = $produk->satuan ? $produk->satuan->name : '';
          $qty = $qtyArray[$index];
          $produk_price = $produk_priceArray[$index];
          $price_subtotal = $qty * $produk_price;

          $pembelian_detail[] = array(
            'produk_id' => $produk_id,
            'produk_name' => $produk_name,
            'kategori' => $kategori,
            'stok' => $stok,
            'satuan' => $satuan,
            'qty' => $qty,
            'produk_price' => $produk_price,
            'price_subtotal' => $price_subtotal,
          );

          // SUM for total
          $price_total += $price_subtotal;

          $index++;
        }
      }
    }

    $data['supplier'] = Supplier::orderBy('id', 'desc')->get();
    $data['produk'] = Produk::orderBy('id', 'desc')->get();
    $data['old_pembelian_detail'] = $pembelian_detail;
    $data['old_price_total'] = $price_total;

    return view ('admin.pages.menus.transaksi.pembelian.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $validator = $this->validate($request, [
      'date' => 'required',
      'suplier_id' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    if ($req['amount_pay'] <= 0) {
      return redirect()->back()->with([
        'success' => false,
        'message' => 'Tidak dapat melakukan pembayaran dengan data minus',
      ]);
    }

    \DB::beginTransaction();

		try {
      $produk_idArray = $req['produk_id'];
      $qtyArray = $req['qty'];
      $produk_priceArray = $req['produk_price'];
      $index = 0;
      $price_total = 0;
      $pembelian_detail = array();
      foreach($produk_idArray as $produkID) {
        $produk = Produk::find($produkID);

        if ($produk) {
          $produk_id = $produk->id;
          $produk_name = $produk->name;
          $qty = $qtyArray[$index];
          $produk_price = $produk_priceArray[$index];
          $price_subtotal = $qty * $produk_price;

          $pembelian_detail[] = array(
            'produk_id' => $produk_id,
            'produk_name' => $produk_name,
            'qty' => $qty,
            'produk_price' => $produk_price,
            'price_subtotal' => $price_subtotal,
          );

          // SUM for total
          $price_total += $price_subtotal;

          // update stok produk
          $produk->stock = $produk->stock + $qty;
          $produk->save();

          $index++;
        }
      }

      if (count($pembelian_detail) <= 0) {
        return redirect()->back()->with([
          'success' => false,
          'message' => 'Produk harus diisi minimal 1 item',
        ])->withInput();
      }

      $code = Pembelian::orderBy('code', 'desc')->first();
      if (!$code) {
        $req['code'] = "BEL1";
      }else {
        $code = $code->code;
        $code = str_replace("BEL", "", $code);
        $req['code'] = "BEL" . ($code + 1);
      }
      $req['pengguna_id'] = auth()->user()->id;
      $req['price_total'] = $price_total;
      $pembelian = Pembelian::create($req);

      foreach($pembelian_detail as $item) {
        $item['pembelian_id'] = $pembelian->id;
        PembelianDetail::create($item);
      }

      // Proses pembayaran
      $code = Pembayaran::orderBy('code', 'desc')->first();
      if (!$code) {
        $code = "BAY1";
      }else {
        $code = $code->code;
        $code = str_replace("BAY", "", $code);
        $code = "BAY" . ($code + 1);
      }
      $reqPembayaran = array(
        'pembelian_id' => $pembelian->id,
        'code' => $code,
        'date' => $req['date'],
        'amount_pay' => $req['amount_pay'],
        'amount_left' => $pembelian->price_total - $req['amount_pay'],
        'amount_total' => $pembelian->price_total,
        'type' => 'pembelian',
        'is_paidoff' => TRUE,
      );
      $pembayaran = Pembayaran::create($reqPembayaran);

			\DB::commit();
			// all good

      if ($pembayaran) {
        return redirect('/transaksi/pembelian/'.$pembelian->id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/pembelian')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function show($id)
  {
    $data['pembelian'] = Pembelian::with('detail','pembayaran')->find($id);
    $data['supplier'] = Supplier::orderBy('id', 'desc')->get();
    $data['produk'] = Produk::orderBy('id', 'desc')->get();

    return view ('admin.pages.menus.transaksi.pembelian.detail', $data);
  }

  public function bayar($id)
  {
    $data['pembelian'] = Pembelian::find($id);

    return view ('admin.pages.menus.transaksi.pembelian.bayar', $data);
  }

  public function bayarSave($id, Request $request)
  {
    $req = $request->except('_token');
    $validator = $this->validate($request, [
      'date' => 'required',
      'amount_pay' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    if ($req['amount_pay'] <= 0) {
      return redirect()->back()->with([
        'success' => false,
        'message' => 'Tidak dapat melakukan pembayaran dengan data minus',
      ]);
    }

    \DB::beginTransaction();

		try {
      $pembelian = Pembelian::find($id);

      // Proses pembayaran
      $code = Pembayaran::orderBy('code', 'desc')->first();
      if (!$code) {
        $code = "BAY1";
      }else {
        $code = $code->code;
        $code = str_replace("BAY", "", $code);
        $code = "BAY" . ($code + 1);
      }
      $reqPembayaran = array(
        'pembelian_id' => $pembelian->id,
        'code' => $code,
        'date' => $req['date'],
        'amount_pay' => $req['amount_pay'],
        'amount_left' => $pembelian->sisa_bayar - $req['amount_pay'],
        'amount_total' => $pembelian->price_total,
        'type' => 'pembelian',
        'is_paidoff' => TRUE,
      );
      $pembayaran = Pembayaran::create($reqPembayaran);

			\DB::commit();
			// all good

      if ($pembayaran) {
        return redirect('/transaksi/pembelian/'.$pembelian->id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/pembelian')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function update($id, Request $request)
  {
    $req = $request->only('date', 'suplier_id');
    $reqDetail = $request->only('produk_id','qty','produk_price');
    $validator = $this->validate($request, [
      'date' => 'required',
      'suplier_id' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    \DB::beginTransaction();

		try {
      $produk_idArray = $reqDetail['produk_id'];
      $qtyArray = $reqDetail['qty'];
      $produk_priceArray = $reqDetail['produk_price'];
      $index = 0;
      $price_total = 0;
      $pembelian_detail = array();
      foreach($produk_idArray as $produkID) {
        $produk = Produk::find($produkID);

        if ($produk) {
          $produk_id = $produk->id;
          $produk_name = $produk->name;
          $qty = $qtyArray[$index];
          $produk_price = $produk_priceArray[$index];
          $price_subtotal = $qty * $produk_price;

          $pembelian_detail[] = array(
            'produk_id' => $produk_id,
            'produk_name' => $produk_name,
            'qty' => $qty,
            'produk_price' => $produk_price,
            'price_subtotal' => $price_subtotal,
          );

          // SUM for total
          $price_total += $price_subtotal;

          $index++;
        }
      }

      if (count($pembelian_detail) <= 0) {
        return redirect()->back()->with([
          'success' => false,
          'message' => 'Produk harus diisi minimal 1 item',
        ])->withInput();
      }

      $req['price_total'] = $price_total;
      $pembelian = Pembelian::where('id', $id)->update($req);

      PembelianDetail::where('pembelian_id', $id)->delete();
      foreach($pembelian_detail as $item) {
        $item['pembelian_id'] = $id;
        PembelianDetail::create($item);
      }

			\DB::commit();
			// all good

      if ($pembelian) {
        return redirect('/transaksi/pembelian/'.$id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/pembelian')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function print($id)
  {
    $data['pembelian'] = Pembelian::with('detail','supplier')->find($id);

    $pdf = \PDF::loadView('admin.pages.menus.transaksi.pembelian.print', $data);
    return $pdf->stream('pembelian_'.$id.'.pdf');
  }
}
