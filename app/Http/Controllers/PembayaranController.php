<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pembayaran;
use App\Models\Penjualan;
use App\Models\Pembelian;
use App\Models\Supplier;
use App\Models\Produk;
use Response;

class PembayaranController extends Controller
{
  public function index()
  {
    $data['data'] = Pembayaran::with('penjualan','pembelian')->orderBy('id', 'desc')->where('amount_left', '>', 0)->get();

    return view('admin.pages.menus.transaksi.pembayaran.table', $data);
  }

  public function create(Request $request)
  {
    $transaksi_idArray = $request->old('transaksi_id');
    $transaksi_typeArray = $request->old('transaksi_type');
    $index = 0;
    $price_total = 0;
    $transaksi = array();
    if ($transaksi_idArray) {
      foreach($transaksi_idArray as $transaksiID) {
        $pembelian = Pembelian::find($transaksiID);

        if ($pembelian) {
          $pembelian_id = $pembelian->id;
          $pembelian_name = $pembelian->name;
          $kategori = $pembelian->kategori ? $pembelian->kategori->name : '';
          $stok = $pembelian->stock;
          $satuan = $pembelian->satuan ? $pembelian->satuan->name : '';
          $qty = $qtyArray[$index];
          $pembelian_price = $pembelian_priceArray[$index];
          $price_subtotal = $qty * $pembelian_price;

          $pembelian[] = array(
            'pembelian_id' => $pembelian_id,
            'pembelian_name' => $pembelian_name,
            'kategori' => $kategori,
            'stok' => $stok,
            'satuan' => $satuan,
            'qty' => $qty,
            'pembelian_price' => $pembelian_price,
            'price_subtotal' => $price_subtotal,
          );

          // SUM for total
          $price_total += $price_subtotal;

          $index++;
        }
      }
    }

    $data['penjualan'] = Penjualan::all();
    $data['pembelian'] = Pembelian::all();
    $data['old_transaksi'] = $transaksi;
    $data['old_price_total'] = $price_total;

    return view ('admin.pages.menus.transaksi.pembayaran.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $validator = $this->validate($request, [
      'date' => 'required',
      'suplier_id' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    \DB::beginTransaction();

		try {
      $produk_idArray = $req['produk_id'];
      $qtyArray = $req['qty'];
      $produk_priceArray = $req['produk_price'];
      $index = 0;
      $price_total = 0;
      $pembayaran_detail = array();
      foreach($produk_idArray as $produkID) {
        $produk = Produk::find($produkID);

        if ($produk) {
          $produk_id = $produk->id;
          $produk_name = $produk->name;
          $qty = $qtyArray[$index];
          $produk_price = $produk_priceArray[$index];
          $price_subtotal = $qty * $produk_price;

          $pembayaran_detail[] = array(
            'produk_id' => $produk_id,
            'produk_name' => $produk_name,
            'qty' => $qty,
            'produk_price' => $produk_price,
            'price_subtotal' => $price_subtotal,
          );

          // SUM for total
          $price_total += $price_subtotal;

          $index++;
        }
      }

      if (count($pembayaran_detail) <= 0) {
        return redirect()->back()->with([
          'success' => false,
          'message' => 'Produk harus diisi minimal 1 item',
        ])->withInput();
      }

      $code = Pembayaran::orderBy('code', 'desc')->first();
      if (!$code) {
        $req['code'] = "BEL1";
      }else {
        $code = $code->code;
        $code = str_replace("BEL", "", $code);
        $req['code'] = "BEL" . ($code + 1);
      }
      $req['pengguna_id'] = auth()->user()->id;
      $req['price_total'] = $price_total;
      $pembayaran = Pembayaran::create($req);

      \DB::commit();
      // all good

      if ($pembayaran) {
        return redirect('/transaksi/pembayaran/'.$pembayaran->id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/pembayaran')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function show($id)
  {
    $data['pembayaran'] = Pembayaran::with('penjualan','pembelian')->find($id);
    $data['penjualan'] = Penjualan::all();
    $data['pembelian'] = Pembelian::all();

    return view ('admin.pages.menus.transaksi.pembayaran.detail', $data);
  }

  public function update($id, Request $request)
  {
    $req = $request->only('date', 'suplier_id');
    $reqDetail = $request->only('produk_id','qty','produk_price');
    $validator = $this->validate($request, [
      'date' => 'required',
      'suplier_id' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    \DB::beginTransaction();

		try {
      $produk_idArray = $reqDetail['produk_id'];
      $qtyArray = $reqDetail['qty'];
      $produk_priceArray = $reqDetail['produk_price'];
      $index = 0;
      $price_total = 0;
      $pembayaran_detail = array();
      foreach($produk_idArray as $produkID) {
        $produk = Produk::find($produkID);

        if ($produk) {
          $produk_id = $produk->id;
          $produk_name = $produk->name;
          $qty = $qtyArray[$index];
          $produk_price = $produk_priceArray[$index];
          $price_subtotal = $qty * $produk_price;

          $pembayaran_detail[] = array(
            'produk_id' => $produk_id,
            'produk_name' => $produk_name,
            'qty' => $qty,
            'produk_price' => $produk_price,
            'price_subtotal' => $price_subtotal,
          );

          // SUM for total
          $price_total += $price_subtotal;

          $index++;
        }
      }

      if (count($pembayaran_detail) <= 0) {
        return redirect()->back()->with([
          'success' => false,
          'message' => 'Produk harus diisi minimal 1 item',
        ])->withInput();
      }

      $req['price_total'] = $price_total;
      $pembayaran = Pembayaran::where('id', $id)->update($req);

			\DB::commit();
			// all good

      if ($pembayaran) {
        return redirect('/transaksi/pembayaran/'.$id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/pembayaran')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function print($id)
  {
    $data['pembayaran'] = Pembayaran::with('detail','supplier')->find($id);

    $pdf = \PDF::loadView('admin.pages.menus.transaksi.pembayaran.print', $data);
    return $pdf->stream('pembayaran_'.$id.'.pdf');
  }
}
