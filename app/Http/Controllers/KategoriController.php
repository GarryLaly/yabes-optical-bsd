<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kategori;
use Response;

class KategoriController extends Controller
{
  public function index()
  {
    $data['data'] = Kategori::orderBy('id', 'desc')->get();

    return view('admin.pages.menus.master.kategori.table', $data);
  }

  public function create()
  {
      return view('admin.pages.menus.master.kategori.entry');
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
        'name' => 'required|unique:kategori|max:30',
    ]);

    $kategori = Kategori::create($req);

    if ($kategori) {
      return redirect('/master/kategori')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/kategori')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function edit($id)
  {
    $data['data'] = Kategori::find($id);
    return view('admin.pages.menus.master.kategori.edit', $data);
  }

  public function update($id, Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
        'name' => 'required|unique:kategori|max:30',
    ]);
    $kategori = Kategori::where('id', $id)->update($req);

    if ($kategori) {
      return redirect('/master/kategori')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/kategori')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function delete($id)
  {
    $kategori = Kategori::find($id);
    $kategori->delete();

    if ($kategori) {
      return redirect('/master/kategori')->with([
        'success' => true,
        'message' => 'Berhasil menghapus data',
      ]);
    }

    return redirect('/master/kategori')->with([
      'success' => false,
      'message' => 'Gagal menghapus data',
    ]);
  }
}
