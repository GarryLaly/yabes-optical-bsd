<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Absensi;
use App\Models\User;
use Response;

class AbsensiController extends Controller
{
  public function index()
  {
    $data['data'] = Absensi::orderBy('id', 'desc')->get();
    if (auth()->user()->role == 'pegawai') {
      $data['data'] = Absensi::where('pengguna_id', auth()->user()->id)->orderBy('id', 'desc')->get();
    }

    return view('admin.pages.menus.absensi.table', $data);
  }

  public function create()
  {
      $data['pegawai'] = User::where('role', 'pegawai')->get();

      return view('admin.pages.menus.absensi.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
        'pengguna_id' => 'required',
        'date' => 'required',
        'time_start' => 'required',
        'time_end' => 'required',
    ]);

    $req['date'] = date('Y-m-d', strtotime($req['date']));
    $req['is_approved'] = 2;
    $kategori = Absensi::create($req);

    if ($kategori) {
      return redirect('/absensi')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/absensi')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function approve($id)
  {
    $absensi = Absensi::find($id);
    $absensi->is_approved = 1;
    $absensi->save();

    return redirect()->back()->with([
      'success' => true,
      'message' => 'Berhasil menyetujui absensi',
    ]);
  }

  public function reject($id)
  {
    $absensi = Absensi::find($id);
    $absensi->is_approved = 0;
    $absensi->save();

    return redirect()->back()->with([
      'success' => true,
      'message' => 'Berhasil menolak absensi',
    ]);
  }
}
