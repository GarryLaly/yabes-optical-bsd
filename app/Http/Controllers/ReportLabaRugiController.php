<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penjualan;
use App\Models\PenjualanDetail;
use App\Models\Pembelian;
use Response;

class ReportLabaRugiController extends Controller
{
  public function index(Request $request)
  {
    $req = $request->all();
    $data['date_range'] = date("Y/m/d") . ' - ' . date("Y/m/d");
    if (isset($req['date_range'])) {
      $data['date_range'] = $req['date_range'];
    }
    $date_array = explode(" - ", $data['date_range']);
    $date_array = preg_replace('/[\W\s\/]+/', '-', $date_array);
    $data['date_start'] = $date_array[0];
    $data['date_end'] = $date_array[1];

    $data['data'] = PenjualanDetail::whereHas('penjualan', function ($query) use ($data) {
        $query->whereBetween('date', [$data['date_start'], $data['date_end']]);
        $query->with('pembayaran');
        $query->orderBy('id', 'desc');
    })->get();

    $data['pembelian'] = Pembelian::whereBetween('date', [$data['date_start'], $data['date_end']])->with('pembayaran')->orderBy('id', 'desc')->get();

    return view('admin.pages.menus.report.labarugi.table', $data);
  }

  public function print(Request $request)
  {
    $req = $request->all();
    $data['date_range'] = date("Y/m/d") . ' - ' . date("Y/m/d");
    if (isset($req['date_range'])) {
      $data['date_range'] = $req['date_range'];
    }
    $date_array = explode(" - ", $data['date_range']);
    $date_array = preg_replace('/[\W\s\/]+/', '-', $date_array);
    $data['date_start'] = $date_array[0];
    $data['date_end'] = $date_array[1];

    $data['data'] = PenjualanDetail::whereHas('penjualan', function ($query) use ($data) {
        $query->whereBetween('date', [$data['date_start'], $data['date_end']]);
        $query->with('pembayaran');
        $query->orderBy('id', 'desc');
    })->get();

    $data['pembelian'] = Pembelian::whereBetween('date', [$data['date_start'], $data['date_end']])->with('pembayaran')->orderBy('id', 'desc')->get();

    $pdf = \PDF::loadView('admin.pages.menus.report.labarugi.print', $data);
    return $pdf->stream('report_labarugi_'.$data['date_start'].'_'.$data['date_end'].'.pdf');
  }
}
