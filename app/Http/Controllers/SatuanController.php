<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Satuan;
use Response;

class SatuanController extends Controller
{
  public function index()
  {
    $data['data'] = Satuan::orderBy('id', 'desc')->get();

    return view('admin.pages.menus.master.satuan.table', $data);
  }

  public function create()
  {
      return view('admin.pages.menus.master.satuan.entry');
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
        'name' => 'required|unique:satuan|max:30',
    ]);
    $satuan = Satuan::create($req);

    if ($satuan) {
      return redirect('/master/satuan')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/satuan')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function edit($id)
  {
    $data['data'] = Satuan::find($id);
    return view('admin.pages.menus.master.satuan.edit', $data);
  }

  public function update($id, Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
        'name' => 'required|unique:satuan|max:30',
    ]);
    $satuan = Satuan::where('id', $id)->update($req);

    if ($satuan) {
      return redirect('/master/satuan')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/satuan')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function delete($id)
  {
    $satuan = Satuan::find($id);
    $satuan->delete();

    if ($satuan) {
      return redirect('/master/satuan')->with([
        'success' => true,
        'message' => 'Berhasil menghapus data',
      ]);
    }

    return redirect('/master/satuan')->with([
      'success' => false,
      'message' => 'Gagal menghapus data',
    ]);
  }
}
