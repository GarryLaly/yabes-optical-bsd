<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request) 
    {
        $credentials = $request->only('username', 'password');
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];

        $validator = \Validator::make($credentials, $rules);
        if($validator->fails()) {
        return json_encode($validator);
            return redirect('login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput($request->except('password'));
        }else {
            $userdata = array(
                'username'     => $request->username,
                'password'  => $request->password
            );

            if (\Auth::attempt($userdata)) {
                return redirect('/');
            }else {
                return redirect('login')->with('message', 'Username atau kata sandi salah');
            }
        }
    }
}
