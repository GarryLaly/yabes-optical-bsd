<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Returjual extends Model
{
    use SoftDeletes;
    protected $table = 'returjual';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'pengguna_id',
        'penjualan_id',
        'code',
        'date',
        'price_total',
    ];

    protected $dates = ['deleted_at'];

    public function penjualan() {
        return $this->belongsTo(Penjualan::class, 'penjualan_id')->with('pelanggan')->withTrashed();
    }

    public function detail() {
        return $this->hasMany(ReturjualDetail::class, 'returjual_id');
    }
}
