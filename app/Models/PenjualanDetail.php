<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenjualanDetail extends Model
{
    protected $table = 'penjualan_detail';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'penjualan_id',
        'produk_id',
        'produk_name',
        'produk_price',
        'qty',
        'price_subtotal',
    ];

    public function penjualan() {
        return $this->belongsTo(Penjualan::class, 'penjualan_id')->withTrashed();
    }

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id')->with('kategori','satuan')->withTrashed();
    }
}
