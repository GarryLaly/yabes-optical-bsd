<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengirimanDetail extends Model
{
    protected $table = 'pengiriman_detail';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'pengiriman_id',
        'penjualan_id',
        'is_delivered',
    ];

    public function pengiriman() {
        return $this->belongsTo(Penjualan::class, 'pengiriman_id')->withTrashed();
    }

    public function penjualan() {
        return $this->belongsTo(Penjualan::class, 'penjualan_id')->with('pelanggan')->withTrashed();
    }
}
